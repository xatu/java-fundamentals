/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zatu.test;

import com.zatu.code.Shipment;
import org.junit.Test;

import static org.zatu.test.DummyData.*;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

/**
 *
 * @author rmezquita
 */
public class ShipmentTest {

    private Shipment shipment = new Shipment();

    @Test
    public void shouldAddItems() throws Exception {
        shipment.add(door);
        shipment.add(window);
        
        assertThat(shipment, contains(door, window));
    }
    
    @Test
    public void shouldReplaceItems() throws Exception {
        shipment.add(door);
        shipment.add(window);
        
        shipment.replace(door, floorPanel);
        
        assertThat(shipment, contains(floorPanel, window));
    }

    @Test
    public void shouldNotReplaceMissingItems() throws Exception {
        shipment.add(window);
        shipment.replace(door, floorPanel);        
        
        assertThat(shipment, contains(window));
    }
    
    @Test
    public void shouldIdentifyVanRequiments() throws Exception {
        shipment.add(door);
        shipment.add(window);
        shipment.add(floorPanel);
        
        shipment.prepare();
        
        assertThat(shipment.getLightVanProducs(), contains(window));
        assertThat(shipment.getHeavyVanProducs(), contains(floorPanel, door));
    }
}
