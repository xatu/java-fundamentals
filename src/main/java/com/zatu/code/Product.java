/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zatu.code;

import java.util.Comparator;
import static java.util.Comparator.comparing;

/**
 *
 * @author rmezquita
 */
public class Product {
    /*
    public static final Comparator<Product> BY_WEIGHT = new Comparator<Product>(){
        public int compare(final Product p1, final Product p2) {
            return Integer.compare(p1.getWeight(), p2.getWeight());
        }
    };
    */
    public static final Comparator<Product> BY_WEIGHT = comparing(Product::getWeight);
    
    private String name;
    private int weight;

    public Product(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }
    
    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }
    
    @Override
    public String toString()
    {
        return "Product{" +
            "name='" + name + '\'' +
            ", weight=" + weight +
            '}';
    }

}
