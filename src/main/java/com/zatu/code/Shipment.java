/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zatu.code;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author rmezquita
 */
public class Shipment implements Iterable<Product> {

    private static final int LIGHT_VAN_MAX_WEIGHT = 20;
    private static final int PRODUCT_NOT_PRESENT = -1;
    private final List<Product> products = new ArrayList<>();
    
    private List<Product> lightVanProducts;
    private List<Product> heavyVanProducts;
    
    public void add(Product product) { products.add(product); }

    public void replace(Product oldProduct, Product newProduct) {        
        final int index = products.indexOf(oldProduct);
        if (index != PRODUCT_NOT_PRESENT ) {
            products.set(index, newProduct);        
        }        
    }

    public void prepare() {
        products.sort(Product.BY_WEIGHT);        
        
        int splitPoint = findSplitPoint();
        
        lightVanProducts = products.subList(0, splitPoint);
        heavyVanProducts = products.subList(splitPoint, products.size());
    }
    
    public List<Product> getHeavyVanProducs() { return heavyVanProducts; }
    
    public List<Product> getLightVanProducs() { return lightVanProducts; }
    
    @Override
    public Iterator<Product> iterator() { return products.iterator(); }

    private int findSplitPoint() {
        for(int i = 0; i < products.size(); i++){
            final Product product = products.get(i);
            if (product.getWeight() > LIGHT_VAN_MAX_WEIGHT){
                return i;
            }
        }
        return 0;
    }

}
